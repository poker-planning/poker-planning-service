package pokerPlanning

import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.ext.bridge.PermittedOptions
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.sockjs.SockJSBridgeOptions
import io.vertx.ext.web.handler.sockjs.SockJSHandler

class MainVerticle : AbstractVerticle() {
  override fun start(startPromise: Promise<Void>) {
    val router = Router.router(vertx)

    val sockJSHandler = SockJSHandler.create(vertx)
    val permittedOptions = PermittedOptions()
      .setAddressRegex("team-.*")
    val options = SockJSBridgeOptions()
      .addInboundPermitted(permittedOptions)
      .addOutboundPermitted(permittedOptions)
    router.mountSubRouter("/eventbus", sockJSHandler.bridge(options))

    vertx
      .createHttpServer()
      .requestHandler(router)
      .listen(8888) { http ->
        if (http.succeeded()) {
          startPromise.complete()
          println("HTTP server started on port 8888")
        } else {
          startPromise.fail(http.cause());
        }
      }
  }
}
